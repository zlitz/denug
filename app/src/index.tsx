import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './app/app';
import './global.less';

ReactDOM.render(
  <App></App>,
    document.getElementById('root')
);