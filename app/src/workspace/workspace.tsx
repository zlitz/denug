import * as React from 'react';
import './workspace.less';
import { Button, AddIcon, DocumentOpenIcon } from 'evergreen-ui';

export class Workspace extends React.Component {

    public override render(): React.ReactNode {
        return (
            <div data-component="workspace">
                {this.makeEmptyWorkspace()}
            </div>
        )
    }

    private makeEmptyWorkspace(): React.ReactNode {
        return (
            <div className="emptyWrapper">
                <div>
                    <Button onClick={() => this.saveFile()} iconBefore={AddIcon} width="200px" >Create New Project...</Button>
                </div>
                <div>
                    <Button  iconBefore={DocumentOpenIcon} width="200px" marginTop="10px">Open Project...</Button>
                </div>
            </div>
        );
    }

    // @ts-ignore
    private async  saveFile() {

        // create a new handle
        const newHandle = await (window as any).showSaveFilePicker();
      
        // create a FileSystemWritableFileStream to write to
        const writableStream = await newHandle.createWritable();
      
        // write our file
        await writableStream.write('foo');
      
        // close the file and write the contents to disk.
        await writableStream.close();



        setInterval(async () => {
            const s = await newHandle.createWritable();
            const d =  Date.now().toString();
            await s.write(d);
            await s.close();
        }, 5000)
      }
}


// // store a reference to our file handle
// let fileHandle;

// async function getFile() {
//   // open file picker
//   [fileHandle] = await (window as any).showOpenFilePicker();

//   if (fileHandle.kind === 'file') {
//     // run file code
//     const file = await fileHandle.getFile();
//     const contents = await file.text();
//     console.log(contents);


//   } else if (fileHandle.kind === 'directory') {
//     // run directory code
//     debugger;
//   }
// }