import * as React from 'react';
import { Workspace } from '../workspace/workspace';
import './app.less';

export class App extends React.Component {

    public override render(): React.ReactNode {
        return (
            <div data-component="App">
                <div id="workspaceContainer">
                    <Workspace></Workspace>
                </div>
            </div>
         );
    }

}