import { Server } from './server';
import * as nPath from 'path';


const projectRoot = nPath.normalize(nPath.join(__dirname, '../../../'));


new Server({
    port: 3000,
    sslCertPath: nPath.join(projectRoot, 'cert/cert.pem'),
    sslKeyPath: nPath.join(projectRoot, 'cert/key.pem')
}).start();