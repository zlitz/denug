import * as express from 'express';

export class VsProjectsController {

    public readonly router: express.Router;

    public constructor() {

        this.router = express.Router();

        this.router.get('/d', (req, res) => {
            res.send('projects router')
        })
    }
}