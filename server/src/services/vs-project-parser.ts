import { parseStringPromise } from 'xml2js';
import * as fs from 'fs-extra';
import * as nPath from 'path';

export class VsProjectParser
{

    public async parseProject(filePath: string): Promise<VsCodeProjectInfo>
    {
        filePath = nPath.normalize(filePath).replace(/\\/g, '/');
        const fileXml = (await fs.readFile(filePath))?.toString();
        const parsed = await parseStringPromise(fileXml, {});

        if (parsed?.Project?.$?.Sdk?.toLowerCase() === 'microsoft.net.sdk')
        {
            return this.parseDotNetSdk(parsed, filePath);
        } else if(parsed?.Project?.$?.ToolsVersion != undefined) {
            return this.parseLegacy(parsed, filePath);
        }


        throw new Error(`Don't know how to parse "${filePath}"`);

    }

    private parseDotNetSdk(obj: any, filePath: string): VsCodeProjectInfo {

        const projectInfo: VsCodeProjectInfo = {
            filePath,
            nugetReferences: [],
            projectReferences: []
        };

        (obj.Project.ItemGroup ?? []).forEach((itemGroup: any) => {

            if (itemGroup?.PackageReference) {
                itemGroup.PackageReference.forEach((packageReference: any) => {
                    projectInfo.nugetReferences.push({
                        name: packageReference.$.Include,
                        version: packageReference.$.Version
                    });
                });
            }
            if (itemGroup?.ProjectReference) {
                itemGroup?.ProjectReference.forEach((projectReference: any) => {
                    projectInfo.projectReferences.push({
                        filePath: nPath.join(filePath, projectReference.$.Include).replace(/\\/g, '/')
                    });
                });
            }
        });

        return projectInfo;
    }

    private parseLegacy(obj: any, filePath: string): VsCodeProjectInfo {

        obj.Project.ItemGroup?.forEach((itemGroup: any) => {
            itemGroup?.Reference?.forEach((reference: any) => {
                const [ name, versionInfo ] = (reference?.$?.Include ?? '').split(',');
                console.log(name, versionInfo)
            });
        })

        if (obj) {}

        const projectInfo: VsCodeProjectInfo = {
            filePath,
            nugetReferences: [],
            projectReferences: []
        };

        return projectInfo;
    }
}


export interface VsCodeProjectInfo
{
    filePath: string;
    nugetReferences: VsCodeProjectNugetReference[];
    projectReferences: VsCodeProjectProjectReference[];
}

export interface VsCodeProjectNugetReference
{
    name: string;
    version: string;
}

export interface VsCodeProjectProjectReference
{
    filePath: string;
}