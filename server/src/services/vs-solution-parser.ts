import { VsCodeProjectInfo, VsProjectParser } from './vs-project-parser';
import * as fs from 'fs-extra';
import * as nPath from 'path';
export class VsSolutionParser {

    public async parseSolution(filePath: string): Promise<VsSolutionInfo>
    {

        const ignoreTypeGuidMap = new Set<string>([
            // Solution folder type
            '2150E333-8FDC-42A3-9474-1A3956D46DE8'
        ]);


        const projectParser = new VsProjectParser();
        const solutionText = (await fs.readFile(filePath)).toString();

        const info: VsSolutionInfo = {
            filePath: nPath.normalize(filePath).replace(/\\/g, '/'),
            projects: []
        };


        const projectStartRegex = /Project\("\{([0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12})\}"\)\s*=\s*/g;
        let match = projectStartRegex.exec(solutionText);
        while (match) {

            const typeGuid = match[1];
            if (ignoreTypeGuidMap.has(typeGuid)) {
                match = projectStartRegex.exec(solutionText);
                continue;
            }

            const projectGuidRegex = /,\s*"\{([0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12})\}"\s*EndProject/gm;
            projectGuidRegex.lastIndex = match.index + match[0].length;

            const projectGuidMatch = projectGuidRegex.exec(solutionText);
            if (projectGuidMatch == undefined) {
                throw new Error(`Could not find project GUID in ${info.filePath}`);
            }
            const projectGuid = projectGuidMatch[1];

            const [ projectName, projectPath ] = solutionText
                .slice(match.index + match[0].length, projectGuidMatch.index)
                .split(',')
                .map(value => value.trim().slice(1, -1));

            const fullProjectPath = nPath.join(nPath.dirname(filePath), projectPath);
            const projectInfo = await projectParser.parseProject(fullProjectPath);

            info.projects.push({
                name: projectName,
                projectGuid,
                typeGuid,
                projectInfo
            });

            match = projectStartRegex.exec(solutionText);
        }

        return info;
    }
}

export interface VsSolutionInfo {
    filePath: string;
    projects: VsSolutionProjectInfo[];
}

export interface VsSolutionProjectInfo {
    name: string;
    typeGuid: string;
    projectGuid: string;
    projectInfo: VsCodeProjectInfo;
}