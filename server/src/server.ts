import * as express from 'express';
import * as https from 'https';
import * as fs from 'fs-extra';
import { VsProjectsController } from './controllers/vs-projects.controller';

export class Server {

    private readonly _config: ServerConfig;
    private readonly _app = express();

    public constructor(config: ServerConfig) {
        this._config = config;

        console.log(config)
    }

    public start() {

        this._app.use('/r1', new VsProjectsController().router);


        this._app.get('/', (req, res) => {
            res.send('gello world');
        });




        https.createServer(
            {
                cert: fs.readFileSync(this._config.sslCertPath),
                key: fs.readFileSync(this._config.sslKeyPath)
            },
            this._app
        )
        .listen(this._config.port, () => {
            console.log(`https://localhost:${this._config.port}`);
        })
    }
}

export type ServerConfig = {
    port: number;
    sslCertPath: string;
    sslKeyPath: string;
}